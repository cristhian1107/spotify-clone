import firebase from 'firebase/compat/app';

const firebaseConfig = {
  apiKey: 'AIzaSyDrijLCWUoWT6ARWT1aQYd86GrOKsnpdyc',
  authDomain: 'spotify-clone-6e3a4.firebaseapp.com',
  projectId: 'spotify-clone-6e3a4',
  storageBucket: 'spotify-clone-6e3a4.appspot.com',
  messagingSenderId: '992113166621',
  appId: '1:992113166621:web:2245ec2a19177656eeb9f4'
};

export default firebase.initializeApp(firebaseConfig);
