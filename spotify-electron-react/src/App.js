import React, { useState } from 'react';
import firebasase from './utils/Firebase';
import 'firebase/compat/auth';

import Auth from './pages/Auth';

function App () {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  firebasase.auth().onAuthStateChanged(currentUser => {
    // console.log(currentUser);
    if (!currentUser) {
      setUser(null);
    } else {
      setUser(currentUser);
    }
    setIsLoading(false);
  });

  if (isLoading) {
    return (null);
  }

  return (!user ? <Auth /> : <UserLogged />);
}

function UserLogged () {
  const logout = () => {
    firebasase.auth().signOut();
  };

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        height: '100vh'
      }}
    >
      <h1>Usuario logeado</h1>
      <button onClick={logout}>Cerrar Sesión</button>
    </div>
  );
}

export default App;
