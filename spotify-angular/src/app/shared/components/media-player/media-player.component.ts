import {Component, OnDestroy, OnInit} from '@angular/core';
import {TrackModel} from '@core/models/tracks.model';
import {MultimediaService} from "@shared/services/multimedia.service";
import {Subscription} from "rxjs"; // TODO: Programación reactiva.

@Component({
  selector: 'app-media-player',
  templateUrl: './media-player.component.html',
  styleUrls: ['./media-player.component.css']
})
export class MediaPlayerComponent implements OnInit, OnDestroy {

  mockCover: TrackModel = {
    _id: 0,
    name: 'BEBE (Oficial)',
    album: 'Gioli & Assia',
    cover: 'https://www.nosequeestudiar.net/site/assets/files/1305/cantante-grabando.jpg',
    url: 'http://localhost/tracks.mp3'
  }

  listObservers$: Array<Subscription> = [];

  constructor(private multimediaService: MultimediaService) {
  }

  // TODO: Ciclo de vida - Primero en ejecutarse
  ngOnInit(): void {
    const observer1$: Subscription = this.multimediaService.callBack.subscribe(
      (response: TrackModel) => {
        console.log('Recibiendo canción ...', response);
      }
    )
    this.listObservers$ = [observer1$];
  }

  // TODO: Ciclo de vida - Último en ejecutarse antes de destruir el componente.
  ngOnDestroy():void {
    this.listObservers$.forEach(u => u.unsubscribe());
    console.log('💥 💣 Media Player destruido');
  }

}
