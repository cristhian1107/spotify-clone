import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  // Solo quiero que funcione en un tag <img/>
  selector: 'img[appImgBroken]'
})
export class ImgBrokenDirective {

  @Input() customImg: string = '';

  /**
   * TODO HostListener esta a la escucha de cualquier evento.
   * Para este caso escucharemos el event 'error' ya que queremos
   * detectar si algún recurso no ha cargado correctamente en el host.
   */
  @HostListener('error') handleError(): void {
    // console.log('🔴 Esta imagen fallo -> ', this.elHost);
    const elNative = this.elHost.nativeElement;
    // elNative.src = '../../../assets/images/error-img.jpg';
    elNative.src = this.customImg;
  };

  /**
   * Method constructor.
   * @param elHost El tag que puede ser manipulado.
   */
  constructor(private elHost: ElementRef) {
    // console.log(this.elHost);
  }

}
