import {Pipe, PipeTransform} from '@angular/core';
import {TrackModel} from "@core/models/tracks.model";

@Pipe({
  name: 'orderList'
})
export class OrderListPipe implements PipeTransform {

  /**
   * Función PIPE que ordena un array según parámetros.
   *
   * @param value Lista a ordenar TrackModel[].
   * @param args Primer argumento.
   * @param sort Tipo de ordenamiento.
   *
   * @return Lista ordenada.
   */
  transform(value: Array<any>, args: string | null = null, sort: string = 'asc'): Array<TrackModel> {
    try {
      if (args === null) {
        return value;
      } else {
        const tmpList = value.sort((a, b) => {
          if (a[args] < b[args]) {
            return -1;
          } else if (a[args] === b[args]) {
            return 0;
          } else if (a[args] > b[args]) {
            return 1;
          }
          return 1;
        });
        return (sort === 'asc') ? tmpList : tmpList.reverse();
      }
    } catch (e) {
      console.log('Algo paso');
      return value
    }
  }

}
