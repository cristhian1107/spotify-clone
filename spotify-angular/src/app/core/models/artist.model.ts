// Informacion que contiene un Artist (artista).
export interface ArtistModel {
  name: string;
  nickname: string;
  nationality: string;
}
