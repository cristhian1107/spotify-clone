import { ArtistModel } from "./artist.model"


// Informacion que contiene un Track (pista o musica).
export interface TrackModel {
  _id: number;
  name: string;
  album: string;
  cover: string;
  duration?: number;
  url: string;
  artist?: ArtistModel;
}
