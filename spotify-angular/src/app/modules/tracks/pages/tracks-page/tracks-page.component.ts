import { Component, OnDestroy, OnInit } from '@angular/core';
import { TrackModel } from '@core/models/tracks.model';
import { TrackService } from "@modules/tracks/services/track.service";
import { Subscription } from "rxjs";

// import * as dataRaw from '../../../../data/tracks.json';


@Component({
  selector: 'app-tracks-page',
  templateUrl: './tracks-page.component.html',
  styleUrls: ['./tracks-page.component.css']
})
export class TracksPageComponent implements OnInit, OnDestroy {

  tracksTrending: Array<TrackModel> = [];
  tracksRandom: Array<TrackModel> = [];
  listObservers$: Array<Subscription> = [];

  constructor(private trackService: TrackService) {

  }

  ngOnInit(): void {
    // TODO: Esto fue migrado al service.
    // Obtener la información de JSON de manera correcta
    // --const { data }: any = (dataRaw as any).default
    // --this.mockTracksList = data;

    const observer1$ = this.trackService.dataTracksTrending$.subscribe(response => {
      this.tracksTrending = response;
      this.tracksRandom = response;
      console.log('🎶 Canciones trending -->', response);
    });

    const observer2$ = this.trackService.dataTracksRandom$.subscribe(response => {
      // this.tracksRandom = response;
      // TODO: Concatenar arrays.
      this.tracksRandom = [...this.tracksRandom, ...response];
      console.log('🎶 Canciones random -->', response);
    });

    this.listObservers$ = [observer1$, observer2$];
  }

  // TODO: Ciclo de vida - Último en ejecutarse antes de destruir el componente.
  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe());
    console.log('💥 💣 Media Player destruido');
  }

}
