import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { TrackModel } from "@core/models/tracks.model";
import * as dataRaw from '../../../data/tracks.json';


@Injectable({
  providedIn: 'root'
})
export class TrackService {

  // A los variables observables se le suele colocar al final $.
  dataTracksTrending$: Observable<TrackModel[]> = of([]);
  dataTracksRandom$: Observable<TrackModel[]> = of([]);

  constructor() {
    const {data}: any = (dataRaw as any).default;
    this.dataTracksTrending$ = of(data);
    this.dataTracksRandom$ = new Observable((observer) => {
      const trackExample: TrackModel = {
        _id: 9,
        name: 'Chop Suey!',
        album: 'Toxicity',
        cover: 'https://lastfm.freetls.fastly.net/i/u/ar0/6c4ee6a22457448f82af23ef5134cfa6.jpg',
        duration: 3.30,
        url: 'https://'
      }

      // observer.next([trackExample])
      setTimeout(() => {
        observer.next([trackExample])
      }, 3500)
    })
  }
}
