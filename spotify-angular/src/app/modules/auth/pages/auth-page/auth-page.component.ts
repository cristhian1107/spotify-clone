import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "@modules/auth/services/auth.service";

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent implements OnInit {

  formLogin: FormGroup = new FormGroup({});

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.formLogin = new FormGroup({
      email: new FormControl('', [
        Validators.required, // Obligatorio.
        Validators.email // Tiene que ser un email.
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ])
    })
  }

  sendLogin(): void {
    //const body = this.formLogin.value;
    //console.log('😿😿😿', body);

    const {email, password} = this.formLogin.value;
    this.authService.sendCredentials(email, password);

  }
}
